#include <LowPower.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <SPI.h>
#include <SD.h>

static const int RXPin = 4, TXPin = 3;
static const uint32_t GPSBaud = 9600;
static const int chipSelect = 10;
const byte LED = 8;
const long InternalReferenceVoltage = 1062;  // Adjust this value to your board's specific internal BG voltage


bool firstloc=false;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

File myFile;

double lat;
double lng;
int turn = 0;

int getBandgap () 
  {
  // REFS0 : Selects AVcc external reference
  // MUX3 MUX2 MUX1 : Selects 1.1V (VBG)  
   ADMUX = bit (REFS0) | bit (MUX3) | bit (MUX2) | bit (MUX1);
   ADCSRA |= bit( ADSC );  // start conversion
   while (ADCSRA & bit (ADSC))
     { }  // wait for conversion to complete
   int results = (((InternalReferenceVoltage * 1024) / ADC) + 5) / 10; 
   return results;
  } // end of getBandgap

void flashBattery ()
  {
  pinMode (LED, OUTPUT);
  for (byte i = 0; i < 10; i++)
    {
    digitalWrite (LED, HIGH);
    delay (50);
    digitalWrite (LED, LOW);
    delay (50);
    }
    
  pinMode (LED, INPUT);
    
  }  // end of flash

void singleflash ()
  {
  pinMode (LED, OUTPUT);
  digitalWrite (LED, HIGH);
  delay (3000);
  pinMode (LED, INPUT);  
  }  // end of flash


// Send a byte array of UBX protocol to the GPS
void sendUBX(uint8_t *MSG, uint8_t len) {
  for(int i=0; i<len; i++) {
    ss.write(MSG[i]);
  }
  //Serial.println();
}

void setup() {
  Serial.begin(9600);
  ss.begin(GPSBaud);
  uint8_t GPSon[] = {0xB5, 0x62, 0x02, 0x41, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x4C, 0x37};
  sendUBX(GPSon, sizeof(GPSon)/sizeof(uint8_t));
  SD.begin();
  getBandgap(); 
  if (getBandgap() < 330) { // If voltage below 3 volts blink led
    flashBattery();
  }else{
    singleflash();
  }
}

void loop() {
  while (ss.available() > 0){
    gps.encode(ss.read());
    if (gps.location.isUpdated()){
      String outfname = String(gps.date.value());
      outfname.concat(".txt");
      myFile = SD.open(outfname, FILE_WRITE);
      // Timestamp
      myFile.print(gps.date.year());
      myFile.print("\t");
      myFile.print(gps.date.month());
      myFile.print("\t");
      myFile.print(gps.date.day());
      myFile.print("\t");
      myFile.print(gps.time.hour());
      myFile.print("\t");
      myFile.print(gps.time.minute());
      myFile.print("\t");
      myFile.print(gps.time.second());
      myFile.print("\t");
      // Latitude in degrees (double)
      lat = gps.location.lat();
      myFile.print(lat, 6);
      myFile.print("\t");
      Serial.print(lat, 6);
      Serial.print("\t");
      // Longitude in degrees (double)
      lng = gps.location.lng();
      myFile.println(lng, 6);
      Serial.println(lng, 6);
      myFile.close();
      firstloc = true;
      turn = 0;
      break;
    }
  } 
  if (firstloc == true) {
    firstloc = false;
    Serial.println("Backup mode");
    Serial.flush();
    //Set GPS to backup mode (sets it to never wake up on its own)
    uint8_t GPSoff[] = {0xB5, 0x62, 0x02, 0x41, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x4D, 0x3B};
    sendUBX(GPSoff, sizeof(GPSoff)/sizeof(uint8_t));
    ss.flush(); //ensure all messages are flushed
    delay(1000);
    ss.end();
    //delay(10000);
    //LowPower.idle(SLEEP_8S, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_OFF, SPI_OFF, USART0_OFF, TWI_OFF);
    for (int i=0; i <= 37; i++) { // sleep for 37 cycles of 8 seconds
      LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
      getBandgap(); 
      if (getBandgap() < 330) { // If voltage below 3.3 volts blink led
        flashBattery();
      }
    }
    ss.begin(GPSBaud); //turn on software serial
    delay(100);
    Serial.println("On");
    //Restart GPS
    uint8_t GPSon[] = {0xB5, 0x62, 0x02, 0x41, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x4C, 0x37};
    sendUBX(GPSon, sizeof(GPSon)/sizeof(uint8_t));
  }
}
